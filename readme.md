<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## About Project
This is a Content Management System which is created by Laravel Framework. The application will allow you to create content by registering, but generally you anonymous user can read posts. It has two authentication role, one is admin and anouther is author. Admin will have to approve author users to get access to use the application. Then author can write content and make any content to his/her favorite list. One of the coolest feature in this application is you can see see how many view achieved each content and able to make content your favorite.   


## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of any modern web application framework, making it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 1100 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## Screenshot

![ScreenShot](/screenshot/screenshot1.png)
![ScreenShot](/screenshot/screenshot2.png)
![ScreenShot](/screenshot/screenshot3.png)
![ScreenShot](/screenshot/screenshot4.png)
![ScreenShot](/screenshot/screenshot5.png)
![ScreenShot](/screenshot/screenshot6.png)

## Security Vulnerabilities

If you discover any error or problems, please send an e-mail to Md. Saimun Hossain via [hossainmdsaimun@gmail.com](mailto:hossainmdsaimun@gmail.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
